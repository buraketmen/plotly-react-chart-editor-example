# Simple `react-chart-editor` example
[`Project-url`](https://github.com/plotly/react-chart-editor/tree/master/examples/demo)

This example built with [`create-react-app`](https://github.com/facebookincubator/create-react-app) uses the `DefaultEditor`, with synchronously-loaded data and a top-level component for state, so as to demo the basic functionality of this component.

## Quick start

```
npm install
npm start
```
